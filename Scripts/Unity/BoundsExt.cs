﻿using UnityEngine;


namespace YOZH.Core.APIExtentions.Unity
{
	public static class BoundsExt
	{
		public static bool Contains(this Bounds bounds, Bounds other)
		{
			return bounds.Contains(other.min) && bounds.Contains(other.max);
		}
	}
}