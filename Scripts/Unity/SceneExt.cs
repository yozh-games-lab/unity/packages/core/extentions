﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class SceneExt
	{
		public static T FindComponentOfType<T>(this Scene scene, bool includeInactive = false) where T : class
		{
			T result = default(T);

			if (scene != default(Scene)) {
				GameObject[] roots = scene.GetRootGameObjects();
				for (int index = 0; index < roots.Length && result == default(T); index++) {
					result = roots[index].GetComponentInChildren<T>(includeInactive);
				}
			}

			return result;
		}

		public static List<T> FindComponentsOfType<T>(this Scene scene, bool includeInactive = false) where T : class
		{
			List<T> result = new List<T>();

			if (scene != default(Scene)) {
				GameObject[] roots = scene.GetRootGameObjects();
				for (int index = 0; index < roots.Length; index++) {
					result.AddRange(roots[index].GetComponentsInChildren<T>(includeInactive));
				}
			}

			return result;
		}
	}
}