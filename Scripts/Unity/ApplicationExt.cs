﻿
namespace YOZH.Core.APIExtentions.Unity
{
	public static class ApplicationUtility
	{
		public static bool IsChangingPlaymode {
			get {
#if UNITY_EDITOR
				return !UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode;
#else
                return !UnityEngine.Application.isPlaying;
#endif
			}
		}
	}
}