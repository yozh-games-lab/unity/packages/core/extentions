﻿using UnityEngine;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class CameraExt
	{
		public static Rect WorldToViewportBounds(this Camera camera, Bounds bounds)
		{
			Vector2[] extentPoints = new Vector2[8]
			{
				camera.WorldToViewportPoint(new Vector3(bounds.min.x, bounds.min.y, bounds.min.z)),
				camera.WorldToViewportPoint(new Vector3(bounds.max.x, bounds.max.y, bounds.max.z)),

				camera.WorldToViewportPoint(new Vector3(bounds.max.x, bounds.min.y, bounds.min.z)),
				camera.WorldToViewportPoint(new Vector3(bounds.min.x, bounds.max.y, bounds.min.z)),
				camera.WorldToViewportPoint(new Vector3(bounds.min.x, bounds.min.y, bounds.max.z)),

				camera.WorldToViewportPoint(new Vector3(bounds.min.x, bounds.max.y, bounds.max.z)),
				camera.WorldToViewportPoint(new Vector3(bounds.max.x, bounds.min.y, bounds.max.z)),
				camera.WorldToViewportPoint(new Vector3(bounds.max.x, bounds.max.y, bounds.min.z))
			};

			Vector2 minExtentPoint = extentPoints[0];
			Vector2 maxExtentPoint = extentPoints[0];
			foreach (Vector2 point in extentPoints) {
				minExtentPoint = Vector2.Min(minExtentPoint, point);
				maxExtentPoint = Vector2.Max(maxExtentPoint, point);
			}
			return new Rect(minExtentPoint.x, minExtentPoint.y, maxExtentPoint.x - minExtentPoint.x, maxExtentPoint.y - minExtentPoint.y);
		}

		public static bool SeesBounds(this Camera camera, Bounds bounds)
		{
			Plane[] frustrumPlanes = GeometryUtility.CalculateFrustumPlanes(camera);
			return GeometryUtility.TestPlanesAABB(frustrumPlanes, bounds);
		}

	}
}