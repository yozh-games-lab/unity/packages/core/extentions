﻿using System;
using UnityEngine;
using YOZH.Core.APIExtentions.DotNET;

namespace YOZH.Core.APIExtentions.Unity
{
	public static class TransformExt
	{
		public static void Reset(this Transform transform)
		{
			transform.localPosition = Vector3.zero;
			transform.rotation = new Quaternion();
			transform.localScale = Vector3.one;
		}

		public static void TransferTo(this Transform transform, Transform target)
		{
			transform.TransferTo(target.position, target.rotation, target.localScale);
		}

		public static void TransferTo(this Transform transform, Vector3 position, Quaternion rotation, Vector3 localScale)
		{
			transform.position = position;
			transform.rotation = rotation;
			transform.localScale = localScale;
		}

		public static void ForEachChild<T>(this Transform transform, Action<T> action) where T : Component
		{
			for (int index = 0; index < transform.childCount; index++) {
				transform.GetChild(index).GetComponents<T>().ForEach(component => action.Invoke(component));
			}
		}

		public static void ForEachRecursive<T>(this Transform transform, Action<T> action) where T : Component
		{
			for (int index = 0; index < transform.childCount; index++) {
				transform.GetChild(index).GetComponentsInChildren<T>().ForEach(component => action.Invoke(component));
			}
		}
	}
}