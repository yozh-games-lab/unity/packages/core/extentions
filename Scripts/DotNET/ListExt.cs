﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YOZH.Core.APIExtentions.DotNET
{
	public static class ListExt
	{
		private static Random Random = new Random();

		public static List<T> DeepClone<T>(this List<T> items, Func<T, T> cloner)
		{
			return new List<T>(items.Select(cloner));
		}

		public static List<T> DeepClone<T>(this List<T> items) where T : ICloneable
		{
			return items.DeepClone(item => (T)item.Clone());
		}

		public static void Shuffle<T>(this List<T> items)
		{
			items.Sort((a, b) => Random.Next(0, 100) < 50 ? -1 : 1);
		}
	}
}